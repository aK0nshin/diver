FROM kcov/kcov AS builder


FROM clux/muslrust
RUN apt-get update -yq && apt install libcurl4-openssl-dev libdw1
COPY --from=builder /usr/local/share/doc/kcov /usr/local/share/doc/kcov
#VOLUME .:/volume
#RUN cargo build

