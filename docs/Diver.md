## Использование
```diver get_k8s [FLAGS] --ci_app <ci_app> --ci_env <ci_env> --image <image> --ingress_hosts <ingress_hosts> --replicas <replicas> --service_name <service_name>```

### OPTIONS
* --ci_app - Опция для линковки деплоймента с GitLab
* --ci_env - Опция для линковки окружения с GitLab
* --image - Полный путь с версиея для Docker образа
* --ingress_hosts - Конфигурация хостов для Ingress (описание смотр ниже)
* --replicas - Кол-во реплик для основного деплоймента
* --service_name - Имя сервиса 
* --tasks_replicas - Кол-во реплик дл тасков (описание смотр ниже)

### FLAGS
* --is_canary - Флаг указывающий нужно ли генерировать конфигурацию для канареек
* --no_tasks - Флаг указывающий не генерировать деплоймент планы для тасков

## User Guide
### Глосарий
* Манифест - файл с деклоративным описанием как и скаикми параметрами мы поедем в кубик
* Основной деплоймент (или деплоймент по умолчанию) - Деплоймент который генерируется по умолчанию и запускается по дефолту из контейнера (обычно апишки или само приложение)
* Таски - Любой деплоймент кроме основного (обычно воркеры, кроны, консьмеры)
* Канарейка - Конфигурация приложения которая раскатывается на определенную кагорту пользователей


### Синтаксис манифеста
Сначала два правила
1. Манифесты никогда не темплейтятся! Если есть необходимость динамических данных в нем, обращайтесь к разработчику и обсудите.
2. Файл манифеста всегда имеет имя `.diver.toml` и должен лежать в корне проекта (на самом деле он должен лежать в тойже директории откуда запускается утилита)

Манифест имеет формат toml. И разбивается на несколько секций, пример манифеста
```toml
version = 1
[global]
envs = [
    'TERM',
    'DB_HOST',
    'SUCK_MY_DICK',
    'DICKEHEAD_MODE',
]
cpu_limit = 600
cpu_request = 500
mem_limit = 800
mem_request = 500
image_pull_secret = "gitlab-registry"

[global.probes.liveness]
type = "http"
path = "/_health"
initial_delay = 4
period = 1
failure_threshold = 2

[global.probes.readiness]
type = "http"
path = "/_health"
initial_delay = 5
period = 1
failure_threshold = 1

[network]
public_port = 80
pod_port = 8788

[task]

[task.worker]
cpu_limit = 900
command = "api_ctl run worker"

[task.cron]
cpu_request = 400
command = "api_ctl run cron"

[task.cron.probes.liveness]
type = "exec"
command = "health.sh"
initial_delay = 4
period = 1

[task.cron.probes.readiness]
type = "exec"
command = "health_readiness.sh"
initial_delay = 4
period = 1


[ingress]
[ingress.saml2]
path = "/saml2/?(.*)"
rewrite_target = "/saml2/$1"

[ingress.saml2.annotations.kubernetes]
enable-cors = "true"

[ingress.saml2.annotations.nginx]
client-max-body-size = "5m"

[ingress.api]
path = "/api/?(.*)"
rewrite_target = "/api/$1"

[ingress.web]
path = "/"

[ingress.ext_name]
path = "/api/ext/?(.*)"
rewrite_target = "/api/ext/$1"
external_name = "api-admin-mr-service.mr-preprod.svc.kube-dev.foo.local"
service_port = 80
```
#### Описание секций
##### Version
Служебное поле с версией манифеста - текущая версия 1
##### Секция global
* envs - список имен переменных которые необходимо будет прокинуть в деплоймент (сука важно - деплоймент не ингрессы не сервисы)
* cpu_request - кол-во милиядер процессора (тип параметра positive integer) выделяемое для основоного деплоймента и если не переопределенно то и для task деплоймента
* cpu_limit(optional) - лимит cpu который может заиспользовать pod (верхний предел), если не указанно то значение высчитывается как cpu_request * 2
* mem_request - аналогично с cpu_request только мегабайты
* mem_limit(optional) - аналагично с cpu_limit только дефолт mem_request * 2
* image_pull_secret = имя секрета для авторизации в docker registry (для гитлаба это обычно gitlab-registry)

##### Секции global.probes.liveness и global.probes.readiness
Секции индетичны по своим полям и отвечают за соотвесвующий тип пробы. Пожалуйста не занижайте значение liveness пробы
* type - Тип проверки (доступные занчения http и exec)
* path(optional) - Путь для проверки, если не указан то /_health. Используется только если тип проверки http
* initial_delay - Через какое время запускать проверку в секундах
* period - Частота проверки в секундах
* failure_threshold(optional) - Количество последовательных неудачных попыток, после которых считаем что под сломался (или не запустился) по умолчанию 15
* command - Комманда для проверки если тип проверки exec

##### Секция network
* public_port - публичный порт который будет слушать ingress (обычно 80)
* pod_port - Порт который слушает приложение в контейнере
##### Секция task.*
* cpu_limit(optional) - аналогично с секцией global 
* cpu_request(optional) - аналогично с секцией global 
* mem_limit(optional) - аналогично с секцией global 
* mem_request(optional) - аналогично с секцией global 

Для каждого параметра выше верно следующее - если параметр не указан то берется значение из global
* command - строка запуска 

##### Секции `tasks.*.probes.liveness` и `tasks.*.probes.readiness`
Тут по аналогии мы можем переопредлить пробы для наших тасков. К сожалению на текущий момент секция не наследуется от 
глобальной в слечае ее переопределения
Т.е. Если мы не укажем секцию для таска то будут использоваться параметры из `global`, если же мы захотим переопределить
 одно или несколько полей из `global` то нам придется переопределять секцию целиком.

##### Секция ingress.*
* path - путь на который терминируется запрос
* rewrite_target - значние для анотации nginx.ingress.kubernetes.io/rewrite-target
* external_name - если указанно - то будет создан сервис с типом ExternalName и и значение его будет равно параметру. Будет создан ингресс который смотрит в этот сервис. Используется для проксирования в другие сервисы или во вне
* service_port - если указанно то ingress будет напралять трраффик на указанный порт сервиса (по умолчанию 80). Работает только External Ingress
##### Секции ingress.<ingress_name>.annotations.[kubernetes|nginx]
Секции для кастомных анотаций для ingress. Все ключи анотации дополняются 
```nginx.ingress.kubernetes.io/``` и ```nginx.org/``` соотвесвенно

### Синтаксис аргумента --ingress_hosts
Объясню на прммере. 

В манифесте имеем 3 ингресса
```toml
[ingress.saml2]
path = "/saml2/?(.*)"
rewrite_target = "/saml2/$1"

[ingress.api]
path = "/api/?(.*)"
rewrite_target = "/api/$1"

[ingress.web]
path = "/"
rewrite_target = "/"
```
Мы хотим что бы 
* ингресс saml2 отвечал по доменным именам bla.preprod.foo.local и zhuk.preprod.foo.local
* ингресс web отвечал по доменному имени bla.preprod.foo.local
* ингресс api отвечал по доменным именам bla.preprod.foo.local и zhuk.preprod.foo.local

Для этого нам нужно в значение аргумента --ingress_hosts передать следующее
```api=bla.preprod.foo.local,zhuk.preprod.foo.local#web=bla.preprod.foo.local#saml2=bla.preprod.foo.local,zhuk.preprod.foo.local```
Формат этой строки следующий
[<ingress_name>=[<dns>,...]#...]

Те для тех кто не понял хосты для ингресса мы передаем в формате my_ingress=доменные имена через запятую, самы же ингрессы разделяются через литерал `#`

Важно! Имена ингрессов должны совпадать с именами ингрессов в манифесте, иначе получим ошибку которая вам раскажет именно об этом.

### Синтаксис аргумента --tasks_replicas
Объясню на прммере. 

В манифесте имеем 2 таска
```toml
[task.worker]
cpu_limit = 900
command = "api_ctl run worker"

[task.cron]
cpu_request = 400
command = "api_ctl run cron"
```
Мы хотим что бы 
* Таск worker быд запущен в 3ех инстансах
* Таск cron в одном

Для этого нам нужно в значение аргумента --tasks_replicas передать следующее
```worker=3#cron=1```
Формат этой строки следующий
[<task_name>=int#...]

Те для тех кто не понял кол-во реплик для таска мы передаем в формате my_task=кол-во реплик, сами же таски разделяются через литерал `#`

Важно! Имена тасков должны совпадать с именами тасков в манифесте, иначе получим ошибку которая вам раскажет именно об этом.


### Прмимеры использования CLI

1. Я хочу раздеплоить все с кронами и шедуллерами на предпрод, основное приложение должно иметь 8 реплик сервис mr-front
```bash
diver get_k8s \ 
--replicas 8 \ 
--tasks_replicas cron=1#worker_1=3#worker_2=2 \
--ci_app core-test-app \ 
--ci_env staging \ 
--image docker-registry.foo.ru/mobile/bla/mr_front/master:650c04e44fe1b0f33fa2cf6f742c7d8acd1f7fa7 \ 
--service_name mr-front \ 
--ingress_hosts api=bla.preprod.foo.local,zhuk.preprod.foo.local#web=bla.preprod.foo.local#saml2=bla.preprod.foo.local,zhuk.preprod.foo
```
2. Я хочу раздеплоить все но без кронов и шедуллеров на ревью, основное приложение должно иметь 1 реплику сервис mr-front
```bash
diver get_k8s \ 
--no_tasks \ 
--tasks_replicas cron=1#worker_1=3#worker_2=2 \ 
--replicas 1 \ 
--ci_app core-test-app \ 
--ci_env review/ONG-1 \ 
--image docker-registry.foo.ru/mobile/bla/mr_front/master:650c04e44fe1b0f33fa2cf6f742c7d8acd1f7fa7 \ 
--service_name mr-front \ 
--ingress_hosts api=ong1-bla.preprod.foo.local,ong1-zhuk.preprod.foo.local#web=ong1-bla.preprod.foo.local#saml2=ong1-bla.preprod.foo.local,ong1-zhuk.preprod.foo
```
3. Я хочу раздеплоить все в прод на канарейку, основное приложение должно иметь 1 реплику сервис mr-front
```bash
diver get_k8s \ 
--no_tasks \ 
--is_canary \ 
--tasks_replicas cron=1#worker_1=3#worker_2=2 \ 
--replicas 1 \ 
--ci_app core-test-app \ 
--ci_env review/ONG-1 \ 
--image docker-registry.foo.ru/mobile/bla/mr_front/master:650c04e44fe1b0f33fa2cf6f742c7d8acd1f7fa7 \ 
--service_name mr-front \ 
--ingress_hosts api=ong1-bla.preprod.foo.local,ong1-zhuk.preprod.foo.local#web=ong1-bla.preprod.foo.local#saml2=ong1-bla.preprod.foo.local,ong1-zhuk.preprod.foo
```

### Примеры типофых конфигураций
1. Я хочу сделать External Ingress который направляет трафик куда-то во вне куба по SSL. В `.diver.toml` мы опишем этот ингресс следующим образом
```toml
[ingress.ext1_name]
path = "/api/ext1/?(.*)"
rewrite_target = "/api/ext1/$1"
external_name = "host.preprod.foo.local"
service_port = 443

[ingress.ext1_name.annotations.nginx]
ssl-services = "ssl-svc"
```
2.  Я хочу сделать External Ingress который направляет трафик куда-то где обслуживается множество хостов (например балансировщик). В `.diver.toml` мы опишем этот ингресс следующим образом
```toml
[ingress.ext1_name]
path = "/api/ext1/?(.*)"
rewrite_target = "/api/ext1/$1"
external_name = "host.preprod.foo.local"

[ingress.ext1_name.annotations.kubernetes]
upstream-vhost = "host.preprod.foo.local"
```
3. У меня жопа (как всегда) и мне нужно предыдущие два пункта сразу.  В `.diver.toml` мы опишем этот ингресс следующим образом
```toml
[ingress.ext1_name]
path = "/api/ext1/?(.*)"
rewrite_target = "/api/ext1/$1"
external_name = "host.preprod.foo.local"
service_port = 443

[ingress.ext1_name.annotations.kubernetes]
upstream-vhost = "host.preprod.foo.local"

[ingress.ext1_name.annotations.nginx]
ssl-services = "ssl-svc"
```

Важно не забывать указывать --no_tasks в канарейках, если шедуллеры и кроны не могут работать одновремннно с разными версиями самих себя же.





