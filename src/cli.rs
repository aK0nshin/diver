use crate::config::{generate_deploy, get_ingresses};
use clap::ArgMatches;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;

pub struct DiverContext<'a> {
    replicas: u8,
    ci_app: &'a str,
    ci_env: &'a str,
    image: &'a str,
    ingress_map: HashMap<String, Vec<String>>,
    tasks_replicas_map: HashMap<String, u8>,
    pub(crate) is_canary: bool,
    pub(crate) service_name: &'a str,
    no_tasks: bool,
}

impl DiverContext<'_> {
    fn from_args<'a>(matches: &'a ArgMatches) -> DiverContext<'a> {
        let is_canary = matches.is_present("is_canary");
        let no_tasks = matches.is_present("no_tasks");
        let service_name = matches.value_of("service_name").unwrap();
        let ingress_hosts = matches.value_of("ingress_hosts").unwrap();
        let replicas = matches.value_of("replicas").unwrap().parse::<u8>().unwrap();
        let ci_app = matches.value_of("ci_app").unwrap();
        let ci_env = matches.value_of("ci_env").unwrap();
        let ingress_map = DiverContext::build_ingress_hosts(&ingress_hosts);
        let image = matches.value_of("image").unwrap();
        let tasks_replicas = matches.value_of("tasks_replicas");
        let tasks_replicas_map = DiverContext::build_tasks_replicas_map(tasks_replicas);
        DiverContext {
            replicas,
            ci_app,
            ci_env,
            image,
            ingress_map,
            is_canary,
            service_name,
            no_tasks,
            tasks_replicas_map,
        }
    }

    pub fn build_ingress_hosts(ingress_hosts: &str) -> HashMap<String, Vec<String>> {
        let mut ingress_hostnames = HashMap::new();
        let r: Vec<&str> = ingress_hosts.split('#').collect();
        for i in &r {
            let mut u: Vec<&str> = i.split('=').collect();
            let t: Vec<&str> = u.drain(1..).collect();
            let hosts_ing: Vec<String> = t[0].split(',').map(|s| s.to_string()).collect();
            ingress_hostnames.insert(u[0].to_owned(), hosts_ing);
        }
        ingress_hostnames
    }

    fn build_tasks_replicas_map(tasks_replicas: Option<&str>) -> HashMap<String, u8> {
        let mut tasks_replicas_map = HashMap::new();
        if let Some(repl_string) = tasks_replicas {
            let r: Vec<&str> = repl_string.split('#').collect();
            for i in &r {
                let mut u: Vec<&str> = i.split('=').collect();
                let t: Vec<&str> = u.drain(1..).collect();
                let task_replica: u8 = t[0].parse::<u8>().unwrap();
                tasks_replicas_map.insert(u[0].to_owned(), task_replica);
            }
            debug!("Tasks replicas={:?}", tasks_replicas_map);
        }
        tasks_replicas_map
    }

    pub fn get_ing_hosts(&self) -> &HashMap<String, Vec<String>, RandomState> {
        &self.ingress_map
    }

    pub fn get_host_by_ing_name(&self, ing_name: &str) -> &Vec<String> {
        self.ingress_map.get(ing_name).unwrap()
    }

    pub fn get_replicas(&self, task_name: Option<&str>) -> u8 {
        if let Some(v) = task_name {
            *self.tasks_replicas_map.get(v).unwrap()
        } else {
            self.replicas
        }
    }

    pub fn get_ci_app(&self) -> String {
        self.ci_app.to_string()
    }

    pub fn get_ci_env(&self) -> String {
        self.ci_env.to_string()
    }

    pub fn get_image(&self) -> String {
        self.image.to_string()
    }

    pub fn is_tasks_allow(&self) -> bool {
        !self.no_tasks
    }
}

pub fn diver_cli(matches: &ArgMatches) {
    let diver_ctx = DiverContext::from_args(matches);
    generate_deploy(".diver.toml", &diver_ctx);
}

pub fn get_ingresses_cli(matches: &ArgMatches) {
    let hostname = matches.value_of("hostname").unwrap();
    get_ingresses(".diver.toml", hostname);
}
