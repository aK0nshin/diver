use std::collections::HashMap;
use std::{fs, io};

use crate::cli::DiverContext;
use crate::k8s::deployment::{
    generate_deployment_manifest, generate_deployment_tasks, ContainerProbe,
    DeploymentTemplateSpecContainersResourceItem, DeploymentTemplateSpecContainersResources,
    ExecContainerProbe, HttpContainerProbe,
};
use crate::k8s::ingress::{generate_ext_ingress_manifest, generate_ingresses_manifest};
use crate::k8s::service::{generate_ext_service_name, generate_service_manifest};
use crate::validators::is_valid_hostname;
use snafu::{ResultExt, Snafu};
use std::io::Write;

#[derive(Debug, Snafu)]
pub enum ConfigError {
    #[snafu(display("Unable to read configuration from {}: {}", kind, source))]
    TOMLError {
        source: toml::de::Error,
        kind: String,
    },

    #[snafu(display("Version of diver doesn't support. Manifest version is {}", version))]
    UnsupportedVersion { version: u16 },

    #[snafu(display("Unable to read configuration from {}: {}", path, source))]
    IOError { source: io::Error, path: String },

    #[snafu(display("Mismatch value for {}", kind))]
    MismatchError { kind: String },

    #[snafu(display("Ingress {} not configured from cli", kind))]
    IngressNotConfigured { kind: String },
}

type Result<T, E = ConfigError> = std::result::Result<T, E>;

impl std::convert::From<toml::de::Error> for ConfigError {
    fn from(e: toml::de::Error) -> Self {
        ConfigError::TOMLError {
            source: e,
            kind: "".to_string(),
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct DiverConfig {
    version: u16,
    global: GlobalConfig,
    network: NetworkConfig,
    task: HashMap<String, TaskConfig>,
    pub(crate) ingress: HashMap<String, IngressConfig>,
}

#[derive(Deserialize, Debug)]
struct NetworkConfig {
    public_port: u16,
    pod_port: u16,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
#[serde(tag = "type")]
enum ProbesMethod {
    Http {
        path: Option<String>,
        initial_delay: u8,
        period: u8,
        failure_threshold: Option<u32>,
    },
    Exec {
        command: String,
        initial_delay: u8,
        period: u8,
        failure_threshold: Option<u32>,
    },
}

#[derive(Deserialize, Debug)]
struct Probe {
    liveness: ProbesMethod,
    readiness: ProbesMethod,
}

#[derive(Deserialize, Debug)]
struct GlobalConfig {
    cpu_limit: Option<u16>,
    cpu_request: u16,
    mem_limit: Option<u16>,
    mem_request: u16,
    image_pull_secret: String,
    envs: Vec<String>,
    probes: Probe,
}

type IngAnnotations = HashMap<String, HashMap<String, String>>;

#[derive(Deserialize, Debug)]
pub struct IngressConfig {
    path: String,
    rewrite_target: Option<String>,
    annotations: Option<IngAnnotations>,
    external_name: Option<String>,
    service_port: Option<u16>,
}

impl IngressConfig {
    pub fn repr_annotations(&self) -> &Option<IngAnnotations> {
        &self.annotations
    }

    pub fn get_external(&self) -> &Option<String> {
        &self.external_name
    }

    pub fn get_service_port(&self) -> &Option<u16> {
        &self.service_port
    }
}

#[derive(Deserialize, Debug)]
struct TaskConfig {
    cpu_limit: Option<u16>,
    cpu_request: Option<u16>,
    mem_limit: Option<u16>,
    mem_request: Option<u16>,
    probes: Option<Probe>,
    command: String,
}

impl TaskConfig {
    fn get_args(&self) -> Vec<&str> {
        self.command.split(' ').collect::<Vec<&str>>()
    }

    fn get_default_mem_limit(&self, global: &GlobalConfig) -> u16 {
        self.mem_request.unwrap_or_else(|| global.mem_request()) * 2
    }

    fn get_default_cpu_limit(&self, global: &GlobalConfig) -> u16 {
        self.cpu_request.unwrap_or_else(|| global.cpu_request()) * 2
    }

    pub fn get_resources(
        &self,
        global: &GlobalConfig,
    ) -> DeploymentTemplateSpecContainersResources {
        let mem_request = self.mem_request.unwrap_or_else(|| global.mem_request());
        let mem_limit = self
            .mem_limit
            .unwrap_or_else(|| self.get_default_mem_limit(global));
        let cpu_request = self.cpu_request.unwrap_or_else(|| global.cpu_request());
        let cpu_limit = self
            .mem_limit
            .unwrap_or_else(|| self.get_default_cpu_limit(global));
        DeploymentTemplateSpecContainersResources {
            requests: DeploymentTemplateSpecContainersResourceItem {
                memory: format!("{}Mi", mem_request),
                cpu: format!("{}m", cpu_request),
            },
            limits: DeploymentTemplateSpecContainersResourceItem {
                memory: format!("{}Mi", mem_limit),
                cpu: format!("{}m", cpu_limit),
            },
        }
    }

    pub fn get_liveness_probe(&self, global: &DiverConfig) -> ContainerProbe {
        let s = self.probes.as_ref().unwrap_or(&global.global.probes);

        return match &s.liveness {
            ProbesMethod::Http {
                path,
                initial_delay,
                period,
                failure_threshold,
            } => ContainerProbe::Http {
                http_get: HttpContainerProbe {
                    path: path.as_ref().unwrap_or(&"_health".to_string()).to_owned(),
                    port: global.network.pod_port,
                },
                period_seconds: *period,
                initial_delay_seconds: *initial_delay,
                failure_threshold: failure_threshold.unwrap_or(15),
            },
            ProbesMethod::Exec {
                command,
                initial_delay,
                period,
                failure_threshold,
            } => ContainerProbe::Exec {
                period_seconds: *period,
                initial_delay_seconds: *initial_delay,
                exec: ExecContainerProbe {
                    command: command.split(' ').map(|val| val.to_string()).collect(),
                },
                failure_threshold: failure_threshold.unwrap_or(15),
            },
        };
    }

    fn get_readiness_probe(&self, global: &DiverConfig) -> ContainerProbe {
        let s = self.probes.as_ref().unwrap_or(&global.global.probes);
        return match &s.readiness {
            ProbesMethod::Http {
                path,
                initial_delay,
                period,
                failure_threshold,
            } => ContainerProbe::Http {
                http_get: HttpContainerProbe {
                    path: path.as_ref().unwrap_or(&"_health".to_string()).to_owned(),
                    port: global.network.pod_port,
                },
                period_seconds: *period,
                initial_delay_seconds: *initial_delay,
                failure_threshold: failure_threshold.unwrap_or(3),
            },
            ProbesMethod::Exec {
                command,
                initial_delay,
                period,
                failure_threshold,
            } => ContainerProbe::Exec {
                period_seconds: *period,
                initial_delay_seconds: *initial_delay,
                exec: ExecContainerProbe {
                    command: command.split(' ').map(|val| val.to_string()).collect(),
                },
                failure_threshold: failure_threshold.unwrap_or(3),
            },
        };
    }
}

pub struct DiverCtx<'a> {
    pub cli: &'a DiverContext<'a>,
    pub config: DiverConfig,
}

impl DiverCtx<'_> {
    fn new<'a>(cli: &'a DiverContext, path: &str) -> Result<DiverCtx<'a>> {
        let config: DiverConfig = DiverConfig::from_path(&path)?;
        config.validate(cli)?;
        Ok(DiverCtx { cli, config })
    }
}

pub fn get_ingresses(path: &str, hostname: &str) {
    let helped_string = gen_ingresses(path, hostname);
    let stdout = io::stdout();
    let mut handle = stdout.lock();

    handle.write_all(helped_string.as_bytes()).unwrap();
    handle.write_all(b"\n").unwrap();
}

fn gen_ingresses(path: &str, hostname: &str) -> String {
    let diver_ctx = DiverConfig::from_path(&path).unwrap_or_else(|e: ConfigError| {
        eprintln!("Error: {}", e);
        std::process::exit(1);
    });

    let mut helped_vec = diver_ctx
        .ingress
        .iter()
        .map(|(k, _)| {
            let mut d = k.clone();
            d.push('=');
            d.push_str(hostname);
            d
        })
        .collect::<Vec<String>>();
    helped_vec.sort();
    helped_vec.join("#")
}

pub fn generate_deploy(path: &str, ctx: &DiverContext) {
    let diver_ctx = DiverCtx::new(ctx, path).unwrap_or_else(|e: ConfigError| {
        eprintln!("Error: {}", e);
        std::process::exit(1);
    });

    // Generate service
    let service = generate_service_manifest(&diver_ctx);

    // Generate ingresses
    let ingresses = generate_ingresses_manifest(&diver_ctx);
    // Generate proxy_ingresses
    let (proxy_ingress, ext_services_names) = generate_ext_ingress_manifest(&diver_ctx);

    // Generate External Services
    let mut ext_services = vec![];
    for (service_name, ext_name) in ext_services_names {
        ext_services.push(generate_ext_service_name(
            &diver_ctx,
            &ext_name,
            &service_name,
        ));
    }
    // Generate main deployment
    let main_depl = generate_deployment_manifest(&diver_ctx, None);

    //Generate tasks deployment
    let mut task_depl: Option<Vec<String>> = None;
    if diver_ctx.cli.is_tasks_allow() {
        task_depl = Some(generate_deployment_tasks(&diver_ctx));
    }
    let stdout = io::stdout();
    let mut handle = stdout.lock();

    handle.write_all(service.as_bytes()).unwrap();
    handle.write_all(b"\n").unwrap();
    handle.write_all(main_depl.as_bytes()).unwrap();
    handle.write_all(b"\n").unwrap();
    for i in ext_services {
        handle.write_all(i.as_bytes()).unwrap();
        handle.write_all(b"\n").unwrap();
    }

    for i in ingresses {
        handle.write_all(i.as_bytes()).unwrap();
        handle.write_all(b"\n").unwrap();
    }
    for i in proxy_ingress {
        handle.write_all(i.as_bytes()).unwrap();
        handle.write_all(b"\n").unwrap();
    }
    if let Some(t) = task_depl {
        for t in t {
            handle.write_all(t.as_bytes()).unwrap();
            handle.write_all(b"\n").unwrap();
        }
    }
}

impl GlobalConfig {
    fn get_default_cpu_limit(&self) -> u16 {
        self.cpu_request * 2
    }

    fn get_default_mem_limit(&self) -> u16 {
        self.mem_request * 2
    }
    pub fn cpu_limit(&self) -> u16 {
        self.cpu_limit
            .unwrap_or_else(|| self.get_default_cpu_limit())
    }

    pub fn cpu_request(&self) -> u16 {
        self.cpu_request
    }

    pub fn mem_limit(&self) -> u16 {
        self.mem_limit
            .unwrap_or_else(|| self.get_default_mem_limit())
    }

    pub fn mem_request(&self) -> u16 {
        self.mem_request
    }
}

impl DiverConfig {
    fn from_path(path: &str) -> Result<DiverConfig> {
        let toml_str = fs::read_to_string(path).context(IOError { path })?;
        let p: DiverConfig = toml::from_str(&toml_str)?;
        if p.version != 1 {
            return Err(ConfigError::UnsupportedVersion { version: p.version });
        }
        Ok(p)
    }

    fn validate(&self, ctx: &DiverContext) -> Result<()> {
        if self.global.cpu_request > self.global.cpu_limit.unwrap_or(self.global.cpu_request) {
            return Err(ConfigError::MismatchError {
                kind: vec![
                    "global.cpu_request value (",
                    &self.global.cpu_request.to_string(),
                    ") grater then global.cpu_limit value (",
                    &self.global.cpu_limit.unwrap().to_string(),
                    ")",
                ]
                .concat(),
            });
        }

        if self.global.mem_request > self.global.mem_limit.unwrap_or(self.global.mem_request) {
            return Err(ConfigError::MismatchError {
                kind: vec![
                    "global.mem_request value (",
                    &self.global.mem_request.to_string(),
                    ") grater then global.mem_limit value (",
                    &self.global.mem_limit.unwrap().to_string(),
                    ")",
                ]
                .concat(),
            });
        }
        self.validate_tasks()?;
        self.validate_ingress(ctx)?;
        Ok(())
    }

    fn validate_tasks(&self) -> Result<()> {
        for (task_name, task) in &self.task {
            if task
                .cpu_request
                .unwrap_or_else(|| self.global.cpu_request())
                > task.cpu_limit.unwrap_or_else(|| self.global.cpu_limit())
            {
                return Err(ConfigError::MismatchError {
                    kind: format!("global.{}.cpu_limit", task_name),
                });
            }
        }
        Ok(())
    }

    fn validate_ingress(&self, ctx: &DiverContext) -> Result<()> {
        let mut paths: HashMap<String, Vec<String>> = HashMap::new();
        for (ingress_name, ingress) in &self.ingress {
            if !ctx.get_ing_hosts().contains_key(ingress_name) {
                return Err(ConfigError::IngressNotConfigured {
                    kind: ingress_name.to_string(),
                });
            };
            if let Some(x) = paths.get_mut(&ingress.path) {
                x.push(ingress_name.parse().unwrap());
            } else {
                paths.insert(ingress.path.to_owned(), vec![ingress_name.to_owned()]);
            }
        }
        let z = paths
            .iter()
            .filter(|&(_k, v)| v.len() > 1)
            .collect::<Vec<_>>();
        if !z.is_empty() {
            return Err(ConfigError::MismatchError {
                kind: format!("path {} duplicate in {:?}", z[0].0, z[0].1),
            });
        }

        for ing_name in self.ingress.keys() {
            let f = ctx.get_host_by_ing_name(ing_name);
            let z = f
                .iter()
                .filter(|v| !is_valid_hostname(v))
                .collect::<Vec<_>>();
            if !z.is_empty() {
                return Err(ConfigError::MismatchError {
                    kind: format!("Hostname {} is wrong in ingress {:?}", z[0], ing_name),
                });
            }
        }

        Ok(())
    }

    pub fn get_cpu_limit(&self) -> u16 {
        self.global.cpu_limit()
    }

    pub fn get_cpu_request(&self) -> u16 {
        self.global.cpu_request()
    }

    pub fn get_mem_limit(&self) -> u16 {
        self.global.mem_limit()
    }

    pub fn get_mem_request(&self) -> u16 {
        self.global.mem_request()
    }

    pub fn get_public_port(&self) -> u16 {
        self.network.public_port
    }

    pub fn get_pod_port(&self) -> u16 {
        self.network.pod_port
    }

    pub fn get_rewrite_target(&self, ing_name: &str) -> String {
        let rewrite = &self.ingress.get(ing_name).unwrap().rewrite_target;
        rewrite.as_ref().unwrap_or(&"/".to_string()).to_string()
    }

    pub fn get_path(&self, ing_name: &str) -> String {
        self.ingress
            .get(ing_name)
            .as_ref()
            .unwrap()
            .path
            .to_string()
    }

    pub fn get_tasks_names(&self) -> Vec<String> {
        self.task.keys().map(|k| k.to_string()).collect::<Vec<_>>()
    }

    pub fn get_envs_names(&self) -> &Vec<String> {
        &self.global.envs
    }

    pub fn get_image_secret(&self) -> &str {
        &self.global.image_pull_secret
    }

    pub fn get_resources(
        &self,
        task_name: Option<&str>,
    ) -> DeploymentTemplateSpecContainersResources {
        if let Some(v) = task_name {
            let task = self.task.get(v).unwrap();
            task.get_resources(&self.global)
        } else {
            DeploymentTemplateSpecContainersResources {
                requests: DeploymentTemplateSpecContainersResourceItem {
                    memory: format!("{}Mi", &self.get_mem_request()),
                    cpu: format!("{}m", &self.get_cpu_request()),
                },
                limits: DeploymentTemplateSpecContainersResourceItem {
                    memory: format!("{}Mi", &self.get_mem_limit()),
                    cpu: format!("{}m", &self.get_cpu_limit()),
                },
            }
        }
    }

    pub fn get_liveness_probe(&self, task_name: Option<&str>) -> ContainerProbe {
        if let Some(v) = task_name {
            let task = self.task.get(v).unwrap();
            task.get_liveness_probe(&self)
        } else {
            return match &self.global.probes.liveness {
                ProbesMethod::Http {
                    path,
                    initial_delay,
                    period,
                    failure_threshold,
                } => ContainerProbe::Http {
                    http_get: HttpContainerProbe {
                        path: path.as_ref().unwrap_or(&"_health".to_string()).to_owned(),
                        port: self.network.pod_port,
                    },
                    period_seconds: *period,
                    initial_delay_seconds: *initial_delay,
                    failure_threshold: failure_threshold.unwrap_or(10),
                },
                ProbesMethod::Exec {
                    command,
                    initial_delay,
                    period,
                    failure_threshold,
                } => ContainerProbe::Exec {
                    period_seconds: *period,
                    initial_delay_seconds: *initial_delay,
                    failure_threshold: failure_threshold.unwrap_or(10),
                    exec: ExecContainerProbe {
                        command: command.split(' ').map(|val| val.to_string()).collect(),
                    },
                },
            };
        }
    }

    pub fn get_readiness_probe(&self, task_name: Option<&str>) -> ContainerProbe {
        if let Some(v) = task_name {
            let task = self.task.get(v).unwrap();
            task.get_readiness_probe(&self)
        } else {
            return match &self.global.probes.readiness {
                ProbesMethod::Http {
                    path,
                    initial_delay,
                    period,
                    failure_threshold,
                } => ContainerProbe::Http {
                    http_get: HttpContainerProbe {
                        path: path.as_ref().unwrap_or(&"_health".to_string()).to_owned(),
                        port: self.network.pod_port,
                    },
                    period_seconds: *period,
                    initial_delay_seconds: *initial_delay,
                    failure_threshold: failure_threshold.unwrap_or(3),
                },
                ProbesMethod::Exec {
                    command,
                    initial_delay,
                    period,
                    failure_threshold,
                } => ContainerProbe::Exec {
                    period_seconds: *period,
                    initial_delay_seconds: *initial_delay,
                    exec: ExecContainerProbe {
                        command: command.split(' ').map(|val| val.to_string()).collect(),
                    },
                    failure_threshold: failure_threshold.unwrap_or(3),
                },
            };
        }
    }

    pub fn get_args(&self, task_name: Option<&str>) -> Option<Vec<&str>> {
        if let Some(v) = task_name {
            let task = self.task.get(v).unwrap();
            Some(task.get_args())
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_gen_ingresses() {
        let ing_host = gen_ingresses("test_data/.diver.toml", "foo.bar.com");
        assert_eq!(
            ing_host,
            "api=foo.bar.com#ext1_name=foo.bar.com#ext_name=foo.bar.com#saml2=foo.bar.com#web=foo.bar.com"
        )
    }

    #[test]
    fn test_global_config_resources_default() {
        let probe = ProbesMethod::Exec {
            command: "123".to_string(),
            initial_delay: 0,
            period: 0,
            failure_threshold: None,
        };
        let probe1 = ProbesMethod::Exec {
            command: "123".to_string(),
            initial_delay: 0,
            period: 0,
            failure_threshold: None,
        };
        let p = GlobalConfig {
            cpu_limit: None,
            cpu_request: 200,
            mem_limit: None,
            mem_request: 345,
            image_pull_secret: "dw".to_string(),
            envs: vec![],
            probes: Probe {
                liveness: probe,
                readiness: probe1,
            },
        };
        assert_eq!(p.get_default_cpu_limit(), 400);
        assert_eq!(p.get_default_mem_limit(), 690);
        assert_eq!(p.cpu_limit(), 400);
        assert_eq!(p.mem_limit(), 690)
    }

    #[test]
    fn test_global_config_resources_not_default() {
        let probe = ProbesMethod::Exec {
            command: "123".to_string(),
            initial_delay: 0,
            period: 0,
            failure_threshold: None,
        };
        let probe1 = ProbesMethod::Exec {
            command: "123".to_string(),
            initial_delay: 0,
            period: 0,
            failure_threshold: None,
        };
        let p = GlobalConfig {
            cpu_limit: Some(300),
            cpu_request: 200,
            mem_limit: Some(400),
            mem_request: 345,
            image_pull_secret: "dw".to_string(),
            envs: vec![],
            probes: Probe {
                liveness: probe,
                readiness: probe1,
            },
        };
        assert_eq!(p.get_default_cpu_limit(), 400);
        assert_eq!(p.get_default_mem_limit(), 690);
        assert_eq!(p.cpu_limit(), 300);
        assert_eq!(p.mem_limit(), 400);
        assert_eq!(p.cpu_request(), 200);
        assert_eq!(p.mem_request(), 345);
    }

    #[test]
    fn test_diver_config() {
        let p = DiverConfig::from_path("test_data/.diver.toml").unwrap();
        assert_eq!(p.get_cpu_limit(), 600);
        assert_eq!(p.get_cpu_request(), 500);
        assert_eq!(p.get_mem_limit(), 800);
        assert_eq!(p.get_mem_request(), 500);
        assert_eq!(p.get_image_secret(), "gitlab-registry");
        assert_eq!(p.get_public_port(), 80);
        assert_eq!(p.get_pod_port(), 8788);
        assert_eq!(p.get_rewrite_target("saml2"), "/saml2/$1");
        assert_eq!(p.get_rewrite_target("web"), "/");
        assert_eq!(p.get_path("saml2"), "/saml2/?(.*)");
        let mut origin_tasks = p.get_tasks_names();
        origin_tasks.sort();
        assert_eq!(origin_tasks, vec!["cron", "worker"]);
        let envs_names = vec![
            "TERM".to_string(),
            "DB_HOST".to_string(),
            "SUCK_MY_DICK".to_string(),
            "DICKEHEAD_MODE".to_string(),
        ];
        assert_eq!(p.get_envs_names(), &envs_names);
    }
}
