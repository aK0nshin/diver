#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub enum CanaryTrack {
    Stable,
    Canary,
}

impl CanaryTrack {
    pub fn value(&self) -> &str {
        match *self {
            CanaryTrack::Stable => "stable",
            CanaryTrack::Canary => "canary",
        }
    }

    pub fn get_track(is_canary: bool) -> Self {
        match is_canary {
            true => CanaryTrack::Canary,
            false => CanaryTrack::Stable,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_track_canary() {
        let track = CanaryTrack::get_track(true);
        assert_eq!("canary".to_string(), track.value());
    }

    #[test]
    fn test_get_track_stable() {
        let track = CanaryTrack::get_track(false);
        assert_eq!("stable".to_string(), track.value());
    }
}
