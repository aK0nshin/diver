use crate::backends::Environment;
use crate::config::DiverCtx;
use crate::k8s::canary::CanaryTrack;
use std::collections::HashMap;

use crate::k8s::ordered_map;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct DeploymentMetadata {
    name: String,
    #[serde(serialize_with = "ordered_map")]
    annotations: HashMap<String, String>,
    #[serde(serialize_with = "ordered_map")]
    labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct DeploymentTemplateMetadata {
    #[serde(serialize_with = "ordered_map")]
    annotations: HashMap<String, String>,
    #[serde(serialize_with = "ordered_map")]
    labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentStrategyRollingUpdate {
    max_unavailable: String,
    max_surge: u16,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentStrategy {
    #[serde(rename(serialize = "type"))]
    strategy_type: String,
    rolling_update: DeploymentStrategyRollingUpdate,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentSelector {
    #[serde(serialize_with = "ordered_map")]
    match_labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplateSpecContainersPorts {
    container_port: u16,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplateSpecContainersEnvs {
    name: String,
    value: String,
}

impl DeploymentTemplateSpecContainersEnvs {
    fn new(name: &str, value: &str) -> DeploymentTemplateSpecContainersEnvs {
        DeploymentTemplateSpecContainersEnvs {
            name: name.to_string(),
            value: value.to_string(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct DeploymentTemplateSpecContainersResourceItem {
    pub(crate) memory: String,
    pub(crate) cpu: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DeploymentTemplateSpecContainersResources {
    pub(crate) requests: DeploymentTemplateSpecContainersResourceItem,
    pub(crate) limits: DeploymentTemplateSpecContainersResourceItem,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HttpContainerProbe {
    pub path: String,
    pub port: u16,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExecContainerProbe {
    pub command: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ContainerProbe {
    #[serde(rename_all = "camelCase")]
    Http {
        http_get: HttpContainerProbe,
        period_seconds: u8,
        initial_delay_seconds: u8,
        failure_threshold: u32,
    },
    #[serde(rename_all = "camelCase")]
    Exec {
        period_seconds: u8,
        initial_delay_seconds: u8,
        exec: ExecContainerProbe,
        failure_threshold: u32,
    },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplateSpecContainers<'a> {
    name: String,
    image: String,
    ports: Vec<DeploymentTemplateSpecContainersPorts>,
    env: Vec<DeploymentTemplateSpecContainersEnvs>,
    resources: DeploymentTemplateSpecContainersResources,
    liveness_probe: ContainerProbe,
    readiness_probe: ContainerProbe,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(borrow)]
    args: Option<Vec<&'a str>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplateSpecImagePullSecrets {
    name: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplateSpec<'a> {
    image_pull_secrets: Vec<DeploymentTemplateSpecImagePullSecrets>,
    #[serde(borrow)]
    containers: Vec<DeploymentTemplateSpecContainers<'a>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct DeploymentTemplate<'a> {
    metadata: DeploymentTemplateMetadata,
    #[serde(borrow)]
    spec: DeploymentTemplateSpec<'a>,
}

#[derive(Debug, Serialize, Deserialize)]
struct DeploymentSpec<'a> {
    strategy: DeploymentStrategy,
    replicas: u8,
    selector: DeploymentSelector,
    #[serde(borrow)]
    template: DeploymentTemplate<'a>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Deployment<'a> {
    api_version: String,
    kind: String,
    metadata: DeploymentMetadata,
    #[serde(borrow)]
    spec: DeploymentSpec<'a>,
}

pub fn generate_deployment_tasks(ctx: &DiverCtx) -> Vec<String> {
    let mut task_depl = vec![];
    for task_name in ctx.config.get_tasks_names() {
        task_depl.push(generate_deployment_manifest(&ctx, Some(&task_name)));
    }
    task_depl
}

pub fn generate_deployment_manifest(ctx: &DiverCtx, dep_name: Option<&str>) -> String {
    let is_canary = ctx.cli.is_canary;
    let mut ann = HashMap::new();
    let mut labels = HashMap::new();
    let mut full_dep_name = [ctx.cli.service_name, "-", dep_name.unwrap_or("main")].concat();
    ann.insert("app.gitlab.com/app".to_string(), ctx.cli.get_ci_app());
    ann.insert("app.gitlab.com/env".to_string(), ctx.cli.get_ci_env());

    if is_canary {
        inject_canary(&mut full_dep_name)
    }
    labels.insert(
        "track".to_string(),
        CanaryTrack::get_track(is_canary).value().to_string(),
    );
    let dep_meta = DeploymentMetadata {
        name: full_dep_name.clone(),
        annotations: ann.clone(),
        labels: labels.clone(),
    };

    let mut ml = HashMap::new();
    ml.insert("app".to_string(), full_dep_name.to_string());
    labels.insert("app".to_string(), full_dep_name.clone());
    let replicas = ctx.cli.get_replicas(dep_name);
    let spec = DeploymentSpec {
        strategy: get_strategy(),
        replicas,
        selector: DeploymentSelector { match_labels: ml },
        template: get_template(ctx, ann, labels, full_dep_name, dep_name),
    };
    let dep = Deployment {
        api_version: "apps/v1".to_string(),
        kind: "Deployment".to_string(),
        metadata: dep_meta,
        spec,
    };
    let yaml = serde_yaml::to_string(&dep);
    yaml.unwrap()
}

fn get_strategy() -> DeploymentStrategy {
    DeploymentStrategy {
        strategy_type: "RollingUpdate".to_string(),
        rolling_update: DeploymentStrategyRollingUpdate {
            max_unavailable: "25%".to_string(),
            max_surge: 1,
        },
    }
}

fn get_template<'a>(
    ctx: &'a DiverCtx,
    ann: HashMap<String, String>,
    labels: HashMap<String, String>,
    full_dep_name: String,
    task_name: Option<&str>,
) -> DeploymentTemplate<'a> {
    let env = get_envs(ctx)
        .iter()
        .map(|(k, v)| DeploymentTemplateSpecContainersEnvs::new(k, v))
        .collect();
    let container = DeploymentTemplateSpecContainers {
        name: full_dep_name,
        image: ctx.cli.get_image(),
        ports: vec![DeploymentTemplateSpecContainersPorts {
            container_port: ctx.config.get_pod_port(),
        }],
        env,
        resources: ctx.config.get_resources(task_name),
        liveness_probe: ctx.config.get_liveness_probe(task_name),
        readiness_probe: ctx.config.get_readiness_probe(task_name),
        args: ctx.config.get_args(task_name),
    };
    let image_pull_secret = DeploymentTemplateSpecImagePullSecrets {
        name: ctx.config.get_image_secret().to_string(),
    };
    DeploymentTemplate {
        metadata: DeploymentTemplateMetadata {
            annotations: ann,
            labels,
        },
        spec: DeploymentTemplateSpec {
            image_pull_secrets: vec![image_pull_secret],
            containers: vec![container],
        },
    }
}

fn get_envs(ctx: &DiverCtx) -> HashMap<String, String> {
    Environment::get_by_keys(ctx.config.get_envs_names())
}

fn inject_canary(full_dep_name: &mut String) {
    full_dep_name.push_str("-canary");
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    #[test]
    fn test_deployment_metadata() {
        let mut annotations = HashMap::new();
        let mut labels = HashMap::new();
        annotations.insert("a".to_string(), "1".to_string());
        labels.insert("la".to_string(), "l1".to_string());
        let dp_mtd = DeploymentMetadata {
            name: "t1".to_string(),
            annotations,
            labels,
        };
        let json_str = "{\"name\":\"t1\",\"annotations\":{\"a\":\"1\"},\"labels\":{\"la\":\"l1\"}}"
            .to_string();
        assert_eq!(serde_json::to_string(&dp_mtd).unwrap(), json_str);
        assert_eq!(
            serde_json::from_str::<DeploymentMetadata>(&json_str).unwrap(),
            dp_mtd
        )
    }

    #[test]
    fn test_template_deployment_metadata() {
        let mut annotations = HashMap::new();
        let mut labels = HashMap::new();
        annotations.insert("a".to_string(), "1".to_string());
        labels.insert("la".to_string(), "l1".to_string());
        let dp_mtd = DeploymentTemplateMetadata {
            annotations,
            labels,
        };
        let json_str = "{\"annotations\":{\"a\":\"1\"},\"labels\":{\"la\":\"l1\"}}".to_string();
        assert_eq!(serde_json::to_string(&dp_mtd).unwrap(), json_str);
        assert_eq!(
            serde_json::from_str::<DeploymentTemplateMetadata>(&json_str).unwrap(),
            dp_mtd
        )
    }
}
