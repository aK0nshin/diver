use crate::config::{DiverCtx, IngressConfig};
use crate::k8s::canary::CanaryTrack;
use crate::k8s::ordered_map;
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
struct IngressMetadata {
    name: String,
    #[serde(serialize_with = "ordered_map")]
    annotations: HashMap<String, String>,
    labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct IngressSpecRulePathBackend {
    service_name: String,
    service_port: u16,
}

#[derive(Debug, Serialize, Deserialize)]
struct IngressSpecRulePath {
    path: String,
    backend: IngressSpecRulePathBackend,
}

#[derive(Debug, Serialize, Deserialize)]
struct IngressSpecRulePaths {
    paths: Vec<IngressSpecRulePath>,
}

#[derive(Debug, Serialize, Deserialize)]
struct IngressSpecRule {
    host: String,
    http: IngressSpecRulePaths,
}

#[derive(Debug, Serialize, Deserialize)]
struct IngressSpec {
    rules: Vec<IngressSpecRule>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Ingress {
    api_version: String,
    kind: String,
    metadata: IngressMetadata,
    spec: IngressSpec,
}

pub fn generate_ingresses_manifest(ctx: &DiverCtx) -> Vec<String> {
    let mut vec_ing = vec![];
    for (k, v) in ctx.config.ingress.iter() {
        if v.get_external().is_none() {
            vec_ing.push(generate_ingress_manifest(&ctx, k, v, None, &None));
        }
    }
    vec_ing
}

pub fn generate_ext_ingress_manifest(ctx: &DiverCtx) -> (Vec<String>, Vec<(String, String)>) {
    let mut vec_ing = vec![];
    let mut vec_ext_service = vec![];
    for (k, v) in ctx.config.ingress.iter() {
        if v.get_external().is_some() {
            let ext_name = v.get_external().as_ref().unwrap().clone();
            vec_ext_service.push((k.clone(), ext_name.clone()));
            vec_ing.push(generate_ingress_manifest(
                &ctx,
                k,
                v,
                Some([k.clone(), "-ext".to_string()].concat()),
                v.get_service_port(),
            ));
        }
    }
    (vec_ing, vec_ext_service)
}

fn generate_ingress_manifest(
    ctx: &DiverCtx,
    ing_name: &str,
    ing: &IngressConfig,
    service_name: Option<String>,
    service_port: &Option<u16>,
) -> String {
    let is_canary = ctx.cli.is_canary;
    let mut ann = HashMap::new();
    let mut labels = HashMap::new();
    let mut full_ing_name;
    if service_name.is_some() {
        full_ing_name = ["proxy", "-", ing_name].concat();
    } else {
        full_ing_name = [ctx.cli.service_name, "-", ing_name].concat();
    }
    let rewrite_target = ctx.config.get_rewrite_target(ing_name);
    ann.insert(
        "nginx.ingress.kubernetes.io/rewrite-target".to_string(),
        rewrite_target,
    );

    if let Some(i) = ing.repr_annotations() {
        inject_kubernetes_annotation(&mut ann, i.get("kubernetes"));
        inject_nginx_annotation(&mut ann, i.get("nginx"));
    }

    if is_canary {
        inject_canary(&mut ann, &mut full_ing_name)
    }
    labels.insert(
        "track".to_string(),
        CanaryTrack::get_track(is_canary).value().to_string(),
    );
    let ingress_meta = IngressMetadata {
        name: full_ing_name,
        annotations: ann,
        labels,
    };
    let mut rule_vec = vec![];
    let service_name = &service_name.unwrap_or_else(|| [ctx.cli.service_name, "-main"].concat());
    for hostname in ctx.cli.get_host_by_ing_name(ing_name) {
        let rule = IngressSpecRule {
            host: hostname.to_string(),
            http: IngressSpecRulePaths {
                paths: vec![IngressSpecRulePath {
                    path: ctx.config.get_path(ing_name).to_string(),
                    backend: IngressSpecRulePathBackend {
                        service_name: service_name.clone(),
                        service_port: service_port.unwrap_or(ctx.config.get_public_port()),
                    },
                }],
            },
        };
        rule_vec.push(rule);
    }

    let spec = IngressSpec { rules: rule_vec };
    let ingress = Ingress {
        api_version: "networking.k8s.io/v1beta1".to_string(),
        kind: "Ingress".to_string(),
        metadata: ingress_meta,
        spec,
    };
    let yaml = serde_yaml::to_string(&ingress);
    yaml.unwrap()
}

fn inject_kubernetes_annotation(
    ann: &mut HashMap<String, String>,
    annotations: Option<&HashMap<String, String>>,
) {
    if let Some(k8s_annot) = annotations {
        for (k, v) in k8s_annot {
            ann.insert(["nginx.ingress.kubernetes.io/", k].concat(), v.to_string());
        }
    }
}

fn inject_nginx_annotation(
    ann: &mut HashMap<String, String>,
    annotations: Option<&HashMap<String, String>>,
) {
    if let Some(k8s_annot) = annotations {
        for (k, v) in k8s_annot {
            ann.insert(["nginx.org/", k].concat(), v.to_string());
        }
    }
}

fn inject_canary(ann: &mut HashMap<String, String>, ing_name: &mut String) {
    ann.insert(
        "nginx.ingress.kubernetes.io/canary".to_string(),
        "true".to_string(),
    );
    ann.insert(
        "nginx.ingress.kubernetes.io/canary-by-header".to_string(),
        "X-KALININ".to_string(),
    );
    ing_name.push_str("-canary");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_inject_canary() {
        let mut ann = HashMap::new();
        ann.insert(
            "nginx.ingress.kubernetes.io/rewrite-target".to_string(),
            "/wee".to_string(),
        );
        let mut ing_name = "test-ing".to_string();
        inject_canary(&mut ann, &mut ing_name);
        assert_eq!("test-ing-canary".to_string(), ing_name);
        assert_eq!(ann["nginx.ingress.kubernetes.io/rewrite-target"], "/wee");
        assert_eq!(ann["nginx.ingress.kubernetes.io/canary"], "true");
        assert_eq!(
            ann["nginx.ingress.kubernetes.io/canary-by-header"],
            "X-KALININ"
        );
    }

    #[test]
    fn test_kubernetes_annotations() {
        let mut ann = HashMap::new();
        let mut annotations = HashMap::new();
        annotations.insert("key".to_string(), "value1".to_string());
        ann.insert("key".to_string(), "value".to_string());
        let ann_some = Some(&annotations);
        inject_kubernetes_annotation(&mut ann, ann_some);
        assert_eq!(ann["key"], "value".to_string());
        assert_eq!(ann["nginx.ingress.kubernetes.io/key"], "value1".to_string())
    }

    #[test]
    fn test_kubernetes_annotations_none() {
        let mut ann = HashMap::new();
        ann.insert("key".to_string(), "value".to_string());
        let ann_some = None;
        inject_kubernetes_annotation(&mut ann, ann_some);
        assert_eq!(ann["key"], "value".to_string())
    }

    #[test]
    fn test_nginx_annotations() {
        let mut ann = HashMap::new();
        let mut annotations = HashMap::new();
        annotations.insert("key".to_string(), "value1".to_string());
        ann.insert("key".to_string(), "value".to_string());
        let ann_some = Some(&annotations);
        inject_nginx_annotation(&mut ann, ann_some);
        assert_eq!(ann["key"], "value".to_string());
        assert_eq!(ann["nginx.org/key"], "value1".to_string())
    }

    #[test]
    fn test_nginx_annotations_none() {
        let mut ann = HashMap::new();
        ann.insert("key".to_string(), "value".to_string());
        let ann_some = None;
        inject_nginx_annotation(&mut ann, ann_some);
        assert_eq!(ann["key"], "value".to_string())
    }
}
