use serde::{Serialize, Serializer};
use std::collections::{BTreeMap, HashMap};

pub mod canary;
pub mod deployment;
pub mod ingress;
pub mod service;

pub fn ordered_map<S>(value: &HashMap<String, String>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let ordered: BTreeMap<_, _> = value.iter().collect();
    ordered.serialize(serializer)
}
