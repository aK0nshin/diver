use crate::config::DiverCtx;
use crate::k8s::canary::CanaryTrack;
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
struct ServiceMetadata {
    name: String,
    labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct ExtServiceMetadata {
    name: String,
    labels: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ServiceSpecPortMap {
    protocol: String,
    port: u16,
    target_port: u16,
}

#[derive(Debug, Serialize, Deserialize)]
struct ServiceSpecSelector<'a> {
    app: &'a str,
    track: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ServiceSpec<'a> {
    #[serde(borrow)]
    selector: ServiceSpecSelector<'a>,
    ports: Vec<ServiceSpecPortMap>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ExtServiceSpec<'a> {
    #[serde(rename(serialize = "type"))]
    #[serde(borrow)]
    service_type: &'a str,
    external_name: &'a str,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Service<'a> {
    api_version: String,
    kind: String,
    metadata: ServiceMetadata,
    #[serde(borrow)]
    spec: ServiceSpec<'a>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ExtService<'a> {
    api_version: String,
    kind: String,
    metadata: ExtServiceMetadata,
    #[serde(borrow)]
    spec: ExtServiceSpec<'a>,
}

pub fn generate_service_manifest(ctx: &DiverCtx) -> String {
    let is_canary = ctx.cli.is_canary;
    let mut service_name = [
        <&str>::clone(&ctx.cli.service_name).to_owned(),
        "-main".to_string(),
    ]
    .concat();
    if is_canary {
        inject_canary(&mut service_name);
    }
    let mut labels = HashMap::new();
    labels.insert(
        "track".to_string(),
        CanaryTrack::get_track(is_canary).value().to_string(),
    );
    let service_meta = ServiceMetadata {
        name: service_name,
        labels,
    };
    let ports = ServiceSpecPortMap {
        protocol: "TCP".to_string(),
        port: ctx.config.get_public_port(),
        target_port: ctx.config.get_pod_port(),
    };
    let spec = ServiceSpec {
        selector: ServiceSpecSelector {
            app: &[ctx.cli.service_name, "-main"].concat(),
            track: CanaryTrack::get_track(is_canary).value().to_string(),
        },
        ports: vec![ports],
    };
    let service = Service {
        api_version: "v1".to_string(),
        kind: "Service".to_string(),
        metadata: service_meta,
        spec,
    };
    let yaml = serde_yaml::to_string(&service);
    yaml.unwrap()
}

pub fn generate_ext_service_name(ctx: &DiverCtx, ext_name: &str, service_name: &str) -> String {
    let is_canary = ctx.cli.is_canary;
    let mut service_name = [service_name, "-ext"].concat();
    if is_canary {
        inject_canary(&mut service_name);
    }
    let mut labels = HashMap::new();
    labels.insert(
        "track".to_string(),
        CanaryTrack::get_track(is_canary).value().to_string(),
    );
    let service_meta = ExtServiceMetadata {
        name: service_name,
        labels,
    };

    let spec = ExtServiceSpec {
        service_type: "ExternalName",
        external_name: ext_name,
    };
    let service = ExtService {
        api_version: "v1".to_string(),
        kind: "Service".to_string(),
        metadata: service_meta,
        spec,
    };
    let yaml = serde_yaml::to_string(&service);
    yaml.unwrap()
}

fn inject_canary(service_name: &mut String) {
    service_name.push_str("-canary");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_inject_canary() {
        let mut service_name = "test-service".to_string();
        inject_canary(&mut service_name);
        assert_eq!("test-service-canary".to_string(), service_name);
    }
}
