#[macro_use]
extern crate log;
extern crate clap;
extern crate serde_json;
extern crate serde_yaml;
#[macro_use]
extern crate serde_derive;
extern crate snafu;
extern crate tera;
extern crate toml;
use clap::{load_yaml, App};

#[cfg(feature = "doc")]
#[macro_use]
extern crate doc_comment;

mod backends;
mod cli;
mod config;
mod k8s;
mod validators;

use crate::cli::get_ingresses_cli;
use cli::diver_cli;

#[cfg(feature = "doc")]
doc_comment!(include_str!("../README.md"), Guide);

fn main() {
    env_logger::init();
    let yaml = load_yaml!("cli.yml");
    let matches_sub = App::from(yaml).get_matches();
    match matches_sub.subcommand().0 {
        "diver" => {
            diver_cli(&matches_sub.subcommand_matches("diver").unwrap());
        }
        "get_ingresses" => {
            get_ingresses_cli(&matches_sub.subcommand_matches("get_ingresses").unwrap());
        }
        _ => println!("{}", matches_sub.usage()),
    };
}
