pub fn is_valid_hostname(hostname: &str) -> bool {
    fn is_alphanumeric(byte: u8) -> bool {
        (byte >= b'a' && byte <= b'z')
            || (byte >= b'A' && byte <= b'Z')
            || (byte >= b'0' && byte <= b'9')
            || byte == b'-'
            || byte == b'.'
    }

    !(hostname.bytes().any(|byte| !is_alphanumeric(byte))
        || hostname.ends_with('-')
        || hostname.starts_with('-')
        || hostname.ends_with('.')
        || hostname.starts_with('.')
        || hostname.is_empty())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_valid_hostname() {
        for hostname in &[
            "VaLiD-HoStNaMe",
            "50-name",
            "235235",
            "some.site.corp.local",
        ] {
            assert!(is_valid_hostname(hostname), "{} is not valid", hostname);
        }
    }

    #[test]
    fn test_is_valid_hostname_wrong() {
        for hostname in &[
            "-invalid-name",
            "also-invalid-",
            "asdf@fasd",
            "@asdfl",
            "asd f@",
            "local.",
            ".some.site.corp.local",
            "some.site.corp.local.",
            "/her.poumi",
        ] {
            assert!(
                !is_valid_hostname(hostname),
                "{} should not be valid",
                hostname
            );
        }
    }
}
