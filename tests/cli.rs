#[cfg(test)]
mod tests {
    use assert_cmd::prelude::*; // Add methods on commands
    use predicates::prelude::*; // Used for writing assertions
    use std::process::Command; // Run programs
    #[test]
    fn success_with_tasks_without_canary() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("diver")?;
        //    2
        cmd.arg("get_k8s")
            .arg("--ingress_hosts")
            .arg("api=foo.com,bar.ru#api1=root.org#saml2=ff.fc#api-ext=root.org")
            .arg("--ci_app")
            .arg("mr-front")
            .arg("--ci_env")
            .arg("staging")
            .arg("--image")
            .arg("image:123")
            .arg("--replicas")
            .arg("2")
            .arg("--tasks_replicas")
            .arg("cron=1#worker=2")
            .arg("--service_name")
            .arg("mr");
        let proxy_api_ingress = r#"
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: proxy-api-ext
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /api/v2/$1
  labels:
    track: stable
spec:
  rules:
    - host: root.org
      http:
        paths:
          - path: /api/v2/?(.*)
            backend:
              serviceName: api-ext-ext
              servicePort: 80
"#;
        let main_service = r#"---
apiVersion: v1
kind: Service
metadata:
  name: mr-main
  labels:
    track: stable
spec:
  selector:
    app: mr-main
    track: stable
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8788"#;

        let main_deployment = r#"---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-main
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: stable
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 2
  selector:
    matchLabels:
      app: mr-main
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-main
        track: stable
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-main
          image: "image:123"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 200m
            limits:
              memory: 512Mi
              cpu: 400m
          livenessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 4
            failureThreshold: 10
          readinessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 5
            failureThreshold: 3"#;

        let ext_service = r#"---
apiVersion: v1
kind: Service
metadata:
  name: api-ext-ext
  labels:
    track: stable
spec:
  type: ExternalName
  externalName: api-admin-mr-service.mr-preprod.svc.kube-dev.foo.local"#;

        let worker_deployment = r#"---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-worker
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: stable
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 2
  selector:
    matchLabels:
      app: mr-worker
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-worker
        track: stable
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-worker
          image: "image:123"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 400m
            limits:
              memory: 512Mi
              cpu: 800m
          livenessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 4
            failureThreshold: 15
          readinessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 5
            failureThreshold: 3
          args:
            - api_ctl
            - run
            - worker"#;

        let cron_deployment = r#"---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-cron
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: stable
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 1
  selector:
    matchLabels:
      app: mr-cron
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-cron
        track: stable
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-cron
          image: "image:123"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 400m
            limits:
              memory: 512Mi
              cpu: 800m
          livenessProbe:
            periodSeconds: 1
            initialDelaySeconds: 4
            exec:
              command:
                - health.sh
            failureThreshold: 15
          readinessProbe:
            periodSeconds: 1
            initialDelaySeconds: 4
            exec:
              command:
                - health_readiness.sh
            failureThreshold: 3
          args:
            - api_ctl
            - run
            - cron
"#;

        let api_ingress = r#"---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: mr-api
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /api/$1
  labels:
    track: stable
spec:
  rules:
    - host: foo.com
      http:
        paths:
          - path: /api/?(.*)
            backend:
              serviceName: mr-main
              servicePort: 80
    - host: bar.ru
      http:
        paths:
          - path: /api/?(.*)
            backend:
              serviceName: mr-main
              servicePort: 80"#;

        cmd.assert()
            .success()
            .stdout(predicate::str::contains(main_service))
            .stdout(predicate::str::contains(main_deployment))
            .stdout(predicate::str::contains(ext_service))
            .stdout(predicate::str::contains(worker_deployment))
            .stdout(predicate::str::contains(cron_deployment))
            .stdout(predicate::str::contains(api_ingress))
            .stdout(predicate::str::contains(proxy_api_ingress));
        // .s()
        // .stderr(predicate::str::contains("Found argument 'test/file/doesnt/exist' which wasn't expected, or isn't valid in this context"));

        Ok(())
    }

    #[test]
    fn success_with_tasks_with_canary() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("config-gen")?;
        //    2
        cmd.arg("diver")
            .arg("--ingress_hosts")
            .arg("api=foo.com,bar.ru#api1=root.org#saml2=ff.fc#api-ext=root.org")
            .arg("--ci_app")
            .arg("mr-front")
            .arg("--ci_env")
            .arg("staging")
            .arg("--image")
            .arg("image:123")
            .arg("--replicas")
            .arg("2")
            .arg("--tasks_replicas")
            .arg("cron=1#worker=2")
            .arg("--service_name")
            .arg("mr")
            .arg("--is_canary");
        let proxy_api_ingress = "
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: proxy-api-ext-canary
  annotations:
    nginx.ingress.kubernetes.io/canary: \"true\"
    nginx.ingress.kubernetes.io/canary-by-header: X-KALININ
    nginx.ingress.kubernetes.io/rewrite-target: /api/v2/$1
  labels:
    track: canary
spec:
  rules:
    - host: root.org
      http:
        paths:
          - path: /api/v2/?(.*)
            backend:
              serviceName: api-ext-ext
              servicePort: 80";
        let main_service = "---
apiVersion: v1
kind: Service
metadata:
  name: mr-main-canary
  labels:
    track: canary
spec:
  selector:
    app: mr-main
    track: canary
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8788";

        let main_deployment = "---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-main-canary
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: canary
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 2
  selector:
    matchLabels:
      app: mr-main-canary
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-main-canary
        track: canary
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-main-canary
          image: \"image:123\"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 200m
            limits:
              memory: 512Mi
              cpu: 400m
          livenessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 4
            failureThreshold: 10
          readinessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 5
            failureThreshold: 3";

        let ext_service = "
---
apiVersion: v1
kind: Service
metadata:
  name: api-ext-ext-canary
  labels:
    track: canary
spec:
  type: ExternalName
  externalName: api-admin-mr-service.mr-preprod.svc.kube-dev.foo.local";

        let worker_deployment = "---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-worker-canary
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: canary
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 2
  selector:
    matchLabels:
      app: mr-worker-canary
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-worker-canary
        track: canary
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-worker-canary
          image: \"image:123\"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 400m
            limits:
              memory: 512Mi
              cpu: 800m
          livenessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 4
            failureThreshold: 15
          readinessProbe:
            httpGet:
              path: /_health
              port: 8788
            periodSeconds: 1
            initialDelaySeconds: 5
            failureThreshold: 3
          args:
            - api_ctl
            - run
            - worker";

        let cron_deployment = "---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mr-cron-canary
  annotations:
    app.gitlab.com/app: mr-front
    app.gitlab.com/env: staging
  labels:
    track: canary
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 1
  replicas: 1
  selector:
    matchLabels:
      app: mr-cron-canary
  template:
    metadata:
      annotations:
        app.gitlab.com/app: mr-front
        app.gitlab.com/env: staging
      labels:
        app: mr-cron-canary
        track: canary
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: mr-cron-canary
          image: \"image:123\"
          ports:
            - containerPort: 8788
          env: []
          resources:
            requests:
              memory: 256Mi
              cpu: 400m
            limits:
              memory: 512Mi
              cpu: 800m
          livenessProbe:
            periodSeconds: 1
            initialDelaySeconds: 4
            exec:
              command:
                - health.sh
            failureThreshold: 15
          readinessProbe:
            periodSeconds: 1
            initialDelaySeconds: 4
            exec:
              command:
                - health_readiness.sh
            failureThreshold: 3
          args:
            - api_ctl
            - run
            - cron
";

        let api_ingress = "---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: mr-api-canary
  annotations:
    nginx.ingress.kubernetes.io/canary: \"true\"
    nginx.ingress.kubernetes.io/canary-by-header: X-KALININ
    nginx.ingress.kubernetes.io/rewrite-target: /api/$1
  labels:
    track: canary
spec:
  rules:
    - host: foo.com
      http:
        paths:
          - path: /api/?(.*)
            backend:
              serviceName: mr-main
              servicePort: 80
    - host: bar.ru
      http:
        paths:
          - path: /api/?(.*)
            backend:
              serviceName: mr-main
              servicePort: 80";

        cmd.assert()
            .success()
            .stdout(predicate::str::contains(format!("{}", main_service)))
            .stdout(predicate::str::contains(format!("{}", main_deployment)))
            .stdout(predicate::str::contains(format!("{}", ext_service)))
            .stdout(predicate::str::contains(format!("{}", worker_deployment)))
            .stdout(predicate::str::contains(format!("{}", cron_deployment)))
            .stdout(predicate::str::contains(format!("{}", api_ingress)))
            .stdout(predicate::str::contains(format!("{}", proxy_api_ingress)));
        // .s()
        // .stderr(predicate::str::contains("Found argument 'test/file/doesnt/exist' which wasn't expected, or isn't valid in this context"));

        Ok(())
    }
}
